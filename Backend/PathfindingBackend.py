from flask import Flask, request
import math
import json
app = Flask(__name__)

class Point:
    def __init__(self, x, y, obstacle):
        self.x = x
        self.y = y
        self.obstacle = obstacle
        self.gCost = 0
        self.hCost = 0
        self.parent = None
    def fCost(self):
        return self.hCost + self.gCost
    def distanceTo(self, other):
        return math.hypot(other.x - self.x, other.y - self.y)

class ConfigSpace:
    def __init__(self, xSize, ySize):
        self.xSize = xSize
        self.ySize = ySize
        self.space = [[Point(x, y, 0) for y in range(self.ySize)] for x in range(self.xSize)]
    def getAdjacentNodes(self, node):
        adjacentNodes = []
        for y in range(-1,2):
            for x in range(-1,2):
                if x != 0 or y != 0:
                    xAdj = x + node.x
                    yAdj = y + node.y
                    if xAdj >= 0 and xAdj < self.xSize and yAdj >= 0 and yAdj < self.ySize:
                        adjacentNodes.append(self.space[xAdj][yAdj])
        return adjacentNodes
    def addObstacle(self, center, width, height):
        app.logger.warning('Obs, %d, %d', center.x, center.y)
        topLeft = Point(center.x - (width / 2), center.y + (height / 2), 1)
        for y in range(topLeft.y - (height - 1), topLeft.y + 1):
            for x in range(topLeft.x, topLeft.x + width):
                if(x < self.xSize and x > 0 and y < self.ySize and y > 0):
                    self.space[y][x].obstacle = 1
                    app.logger.warning('Obs at %d, %d: %d', x, y, self.space[y][x].obstacle)
    def debugDrawSpace(self):
        for y in range(0, self.ySize):
            row = ""
            for x in range(0, self.xSize):
                row += str(self.space[y][x].obstacle) + " "
            app.logger.warning(row)


def findPath(src, dest, activeSpace):
    if(dest.x >= activeSpace.xSize or dest.x < 0 or dest.y >= activeSpace.ySize or dest.y < 0):
        return []

    openList = []
    closedList = []

    openList.append(src)

    app.logger.warning('Src: %d, %d', src.x, src.y)
    app.logger.warning('Dst: %d, %d', dest.x, dest.y)
    while len(openList) > 0:
        node = openList[0]
        for comparNode in openList:
            if comparNode.fCost() < node.fCost() or comparNode.fCost() == node.fCost():
                if comparNode.hCost < node.hCost: 
                    node = comparNode

        openList.remove(node)
        closedList.append(node)

        if node.x == dest.x and node.y == dest.y:
            app.logger.warning('Found path')
            return retracePath(src, node)

        for adjacent in activeSpace.getAdjacentNodes(node):
            if adjacent.obstacle != 1 and adjacent not in closedList:
                newCostToAdj = node.gCost + node.distanceTo(adjacent)
                if newCostToAdj < adjacent.gCost or adjacent not in openList:
                    adjacent.gCost = newCostToAdj
                    adjacent.hCost = adjacent.distanceTo(dest)
                    adjacent.parent = node;
                    if adjacent not in openList:
                        openList.append(adjacent)


def retracePath(start, end):
    path = []
    current = end
    while current != start:
        path.append(current)
        current = current.parent
    path.reverse()
    return path

cSpace = ConfigSpace(40, 40)
cSpace.addObstacle(Point(10, 10, 1), 5, 5)

cSpace.debugDrawSpace();

@app.route('/getpath', methods = ['POST'])
def sendpath():
    x = int(request.form['x'])
    y = int(request.form['y'])
    app.logger.warning('POST: %d, %d', x, y)
    to_target = findPath(Point(0, 0, 0), Point(x, y, 0), cSpace)
    path_list = []
    for n in to_target:
        path_list.append((n.x, n.y))
    return json.dumps(path_list)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=6702)

