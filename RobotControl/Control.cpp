#include <iostream>
#include "Drive.hpp"

int main(int argc, char *argv[])
{
    char *url = "http://sovnik.com:6702/getpath";

    if(argc != 3) {
        std::cout << "Usage ./dogfind x y" << std::endl;
        return 1;
    }

    

    Drive::init();
    int x = atoi(argv[1]);
    int y = atoi(argv[2]);
    std::vector<Point> path = getpath(url, Point{x, y});
    Drive::goDistance(.8f, 2);
    gpioTerminate();
    return 0;
}
