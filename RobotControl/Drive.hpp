#include <iostream>
#include <chrono>

#include <math.h>
#include <stdio.h>
#include <pigpio.h>
#include "PathClient.hpp"

#define PI 3.141592653589793

// Big baka
#define MOTOR_LEFT_PIN 19
#define MOTOR_RIGHT_PIN 12

//Hardware Constraints

#define REDUCTION_MULTIPLIER .375
const double max_v = .80;

float clamp(float p, float min, float max) {
    float power = std::max(min, std::min(p, max));
    return power;
}

double getVelocity(float power) {
    return clamp(power, 0, 1) * max_v;
}

Point rotate(Point p, double theta) {
    double angle = theta * PI / 180;
    double x = p.x * cos(angle) - p.y * sin(angle);
    double y = p.x * sin(theta) + p.y * cos(angle);
    Point rotated{x, y};
    return rotated;
}

namespace Drive {
class DcMotor {
public:
    DcMotor(int gpio_pin)
        : pin{gpio_pin}
    {
        power = 0;
        gpioSetMode(pin, PI_OUTPUT);
    }
    void setPower(float p) {
        power = clamp(p, 0, 1);
        gpioPWM(pin, scaledPower(power));
    }
    void stopMotor() {
        setPower(0);
    }
    int getPin() { return pin; }
private:
    int pin;
    float power;
    float scaledPower(float p) {
        float adjusted_power = p * 255;
        return adjusted_power;
    }
};

class DriveTimer {
public:
    DriveTimer()
        : start{start = std::chrono::high_resolution_clock::now()}
    {
    }
    double duration() {
        using namespace std::chrono;
        return (duration_cast<seconds>(high_resolution_clock::now() - start)).count();
    }
private:
    std::chrono::high_resolution_clock::time_point start;
};

struct Pose {
    double x;
    double y;
    double theta;
    void addVector(double v_x, double v_y) {
        x += v_x;
        y += v_y;
    }
    double getDistance(Point p) {
        std::cout << "X: " << p.x << " Y: " << p.y << std::endl;
        double dist = hypot(p.x - x, p.y - y);
        std::cout << "Distance: " << dist << std::endl;
        return dist;
    }
    double getAngle(Point p) {
        double angle = (atan2(p.x - x, p.y - y) * 180 / PI);
        std::cout << "Angle: " << angle << std::endl;
        return angle;
    }
};

Pose robotPose{50, 50, 0};

int init() {
    if (gpioInitialise() < 0)
    {
        fprintf(stderr, "GPIO failed, onii-chan\n");
        return 1;
    }
}

DcMotor motorLeft(MOTOR_LEFT_PIN);
DcMotor motorRight(MOTOR_RIGHT_PIN);

void goDistance(float power, double distance) {
    double driveTime = distance / getVelocity(power);
    DriveTimer dt;
    while (dt.duration() < driveTime) {
        motorLeft.setPower(power * REDUCTION_MULTIPLIER);    
        motorRight.setPower(power);    
    }
    motorLeft.stopMotor();
    motorRight.stopMotor();
    Point chance{distance * 10, 0};
    Point rotated = rotate(chance, robotPose.theta);
    robotPose.addVector(rotated.x, rotated.y);
    std::cout << "Distance: " << distance << std::endl;
    std::cout << "Robot Coordinates: " << robotPose.x << ", " << robotPose.y << std::endl;
}

void turnLeft(float power, double degrees) {
    double driveTime = degrees / (2 * getVelocity(power));
    DriveTimer dt;
    while (dt.duration() < driveTime){
        motorRight.setPower(power);    
    }
    motorRight.stopMotor();
    robotPose.theta += degrees;
}

void turnRight(float power, double degrees) {
    double driveTime = degrees / (2 * getVelocity(power));
    DriveTimer dt;
    while (dt.duration() < driveTime) {
        motorLeft.setPower(power);    
    }
    motorLeft.stopMotor();
    robotPose.theta -= degrees;
}

void moveTo(float power, Point p) {
    double d_theta = robotPose.getAngle(p) - robotPose.theta;
    if(d_theta < 0) turnRight(d_theta, power);
    else turnLeft(d_theta, power);
    goDistance(power, robotPose.getDistance(p) / 10);
}
}
