#include <iostream>
#include <vector>
#include <stdio.h>
#include <curl/curl.h>
#include "json.hpp"

using json = nlohmann::json;

static char errorBuffer[CURL_ERROR_SIZE];
static std::string buffer;

struct Point {
    int x;
    int y;
};

static int writer(char *data, size_t size, size_t nmemb, std::string *writerData)
{
    if (writerData == NULL) return 0;

    writerData->append(data, size*nmemb);
    return size*nmemb;
}

static bool init(CURL *&conn, char *url)
{
    CURLcode code;
 
    conn = curl_easy_init();

    if(conn == NULL) {
        fprintf(stderr, "Failed to create CURL connection\n");
        exit(EXIT_FAILURE);
    }

    code = curl_easy_setopt(conn, CURLOPT_ERRORBUFFER, errorBuffer);
    if(code != CURLE_OK) {
        fprintf(stderr, "Failed to set error buffer [%d]\n", code);
        return false;
    }

    code = curl_easy_setopt(conn, CURLOPT_URL, url);
    if(code != CURLE_OK) {
        fprintf(stderr, "Failed to set URL [%s]\n", errorBuffer);
        return false;
    }

    code = curl_easy_setopt(conn, CURLOPT_FOLLOWLOCATION, 1L);
    if(code != CURLE_OK) {
        fprintf(stderr, "Failed to set redirect option [%s]\n", errorBuffer);
        return false;
    }

    code = curl_easy_setopt(conn, CURLOPT_WRITEFUNCTION, writer);
    if(code != CURLE_OK) {
        fprintf(stderr, "Failed to set writer [%s]\n", errorBuffer);
        return false;
    }

    code = curl_easy_setopt(conn, CURLOPT_WRITEDATA, &buffer);
    if(code != CURLE_OK) {
        fprintf(stderr, "Failed to set write data [%s]\n", errorBuffer);
        return false;
    }

    return true;
}

std::vector<Point> getpath(char *url, Point coordinates)
{
    CURL *conn = NULL;
    CURLcode code;

    curl_global_init(CURL_GLOBAL_DEFAULT);
 

    if(!init(conn, url)) {
        fprintf(stderr, "Connection initializion failed\n");
        exit(EXIT_FAILURE);
    }
 
    std::string postfields = "x=" + std::to_string(coordinates.x) + "&y=" + std::to_string(coordinates.y);
    curl_easy_setopt(conn, CURLOPT_POSTFIELDS, postfields.c_str());
    code = curl_easy_perform(conn);
    curl_easy_cleanup(conn);

    std::string path_raw = buffer;
    json j = json::parse(path_raw);

    std::vector<Point> point_path;
    for (int i = 0; i < j.size(); i++) {
        std::cout << "Path X: " << j[i][0] << " Y: " << j[i][1] << std::endl;
        point_path.push_back(Point{j[i][0], j[i][1]});
    }
    return point_path;
}
